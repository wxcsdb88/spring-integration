<%--
  Created by IntelliJ IDEA.
  User: wangxin
  Date: 2017/5/9
  Time: 19:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  <title>当前时间</title>
  <script type="text/javascript" language="JavaScript">
      window.onload = function () {
          var show = document.getElementById("show");
          setInterval(function () {
              var time = new Date();
              // 程序计时的月从0开始取值后+1
              var m = time.getMonth() + 1;
              var t = time.getFullYear() + "-" + m + "-"
                  + time.getDate() + " " + time.getHours() + ":"
                  + time.getMinutes() + ":" + time.getSeconds();
              show.innerHTML = t;
          }, 1000);
      };
  </script>
</head>
<body onload="showLeftTime()">
<div id="show"></div>
</body>
</html>
