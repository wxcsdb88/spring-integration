package com.futurever.sm.service;

import com.futurever.sm.model.User;

import java.util.List;

/**
 * Created by wxcsdb88 on 2017/5/9 20:17.
 */
public interface UserService {
    List<User> getAllUser();

    User getUserByPhoneOrEmail(String emailOrPhone, Short state);

    User getUserById(Long userId);
}
