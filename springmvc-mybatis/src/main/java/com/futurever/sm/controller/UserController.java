package com.futurever.sm.controller;

import com.futurever.sm.model.User;
import com.futurever.sm.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by wxcsdb88 on 2017/5/9 20:19.
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private static Logger log = Logger.getLogger(UserController.class);
    @Autowired
    private UserService userService;

    @RequestMapping("/showUser")
    public String showUser(Model model) {
        log.info("查询所有用户信息");
        List<User> userList = userService.getAllUser();
        model.addAttribute("userList", userList);
        return "showUser";
    }

    @RequestMapping(value = "/findById/{id}", method = RequestMethod.GET)
    public String findUser(Model model, @PathVariable(value = "id") Long id) {
        User user = userService.getUserById(id);
        log.warn(user.getId() + " : " + user.getUserName());
        model.addAttribute("user_nickname", user.getUserName());
        return "user";
    }
}
